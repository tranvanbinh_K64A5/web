<?php
session_start();
?>

<!DOCTYPE html>
<html lang="vn">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstra Datepicker CSS -->
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">


</head>

<body>


    <form style=" width: 630px; margin: auto; padding-bottom: 20px; margin-top: 50px;" name="" action=""
        enctype="multipart/form-data" method="post">

        <?php
        //khai báo biến nhớ không bị mất đi khi reload trang
        $tukhoa = "";
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $phankhoa = isset($_POST['khoa']) ? $_POST['khoa']:"";
            $tukhoa = isset($_POST['tukhoa']) ? $_POST['tukhoa']:"";
        }
        
        $DataSinhVien = array(
            array("1", "Nguyễn Văn A", "Khoa học máy tính"),
            array("2", "Trần Thị B", "Khoa học máy tính"),
            array("3", "Nguyễn Hoàng C", "Khoa học vật liệu"),
            array("4", "Đinh Quang D", "Khoa học vật liệu")
        );

        $phan_khoa = array(
            "empty" => "",
            "MAT" => "Khoa học máy tính",
            "KDL" => "Khoa học vật liệu");


        $DisplaySinhVien = array();
        //xử lí danh sách sinh viên hiển thị ra màn hình
        if (!isset($_POST["khoa"]) or (isset($_POST["khoa"]) and $_POST["khoa"] == "empty")){
            $DisplaySinhVien = $DataSinhVien;
        } else {
            $DisplaySinhVien = array();
            for ($i=0; $i<count($DataSinhVien); $i++){
                if ($DataSinhVien[$i][2] == $phan_khoa[$_POST["khoa"]]){
                    array_push($DisplaySinhVien, array($DataSinhVien[$i][0],$DataSinhVien[$i][1],$DataSinhVien[$i][2]));
                }
            }
        }

        ?>


        <?php 
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            //xử lí sự kiện nút thêm
            if(isset($_POST['them'])) {
                echo "<script> location.href='dangki.php'; </script>";
                exit;
            }
        }
        ?>



        <div class="container"
            style="display: flex;  justify-content: center;   align-items: center; margin-top: 20px;">
            <font face="Arial" style=" width: 120px; height: 40px; padding: 10px; ">
                Khoa</font>
            <div style='width: 330px; '>
                <select name="khoa" id="phankhoa"
                    style='width: 190px; height : 40px; border: 2px solid #4f7ba3; background-color: #e1eaf4;'
                    value="<?php echo $phankhoa;?>">
                    <?php 
                    $count = count($phan_khoa);
                    foreach($phan_khoa as $key => $value){
                        if ($key == $phankhoa){
                            echo '<option  selected="selected" value="',$key,'">',$value,'</option>';
                        } else {
                            echo '<option  value="',$key,'">',$value,'</option>';
                        }
                    }
                   ?>
                </select>
            </div>
        </div>

        <div class="container"
            style="display: flex;  justify-content: center;   align-items: center; margin-top: 20px;">
            <font face="Arial" style="width: 120px; height: 40px;  padding: 10px;">
                Từ khóa</font>
            <div style='width: 330px;  '>
                <input type="text" name="tukhoa" id="tukhoa" value="<?php echo $tukhoa;?>"
                    style=" height: 40px; border: 2px solid #4f7ba3; width: 190px;  background-color: #e1eaf4;">
            </div>
        </div>

        <div style="display: flex;align-items: center; margin-top: 20px;">
            <button type="button" style="background-color: #4f81bd; border: 2px solid #395e8d; color: white; margin-left: 160px;"  onclick="xoa()">
                        Xóa</button>
            <button name="timkiem"
                style="background-color: #4f81bd; border: 2px solid #395e8d; color: white;  width: 120px;  height: 40px; margin-left: 40px;  border-radius: 8px; margin-top: 10px;">
                Tìm kiếm </button>
        </div>




        <div style="display: flex; margin-top: 20px;  justify-content: space-between;">

            <font face="Arial" style="height: 40px;  padding: 10px;">
                Số sinh viên tìm thấy: <?php echo count($DisplaySinhVien); ?></font>


            <button name="them"
                style="background-color: #4f81bd; border: 2px solid #395e8d; color: white;  width: 80px;  height: 35px; border-radius: 8px; margin-right: 25px;">
                Thêm</button>
        </div>




        <table style="margin: auto; margin-top: 20px; ">
            <tr>
                <td style="width:50px">No</td>
                <td style="width:150px">Tên sinh viên</td>
                <td style="width:300px">Khoa</td>
                <td style="width:100px" colspan="2">Action</td>
            </tr>

            <?php  
            $lenData = count($DisplaySinhVien);
            for ($i=0; $i < $lenData; $i++){
                echo '<tr>
                <td>'.$DisplaySinhVien[$i][0].'</td>
                <td>'.$DisplaySinhVien[$i][1].'</td>
                <td>'.$DisplaySinhVien[$i][2].'</td>
                <td>
                <button style="background-color: #92b1d6; border: 2px solid #395e8d; color: white;" onclick="xoa()">
                        Xóa</button>
                </td>
                <td>
                    <button style="background-color: #92b1d6; border: 2px solid #395e8d; color: white;">
                        Sửa</button>
                </td>
            </tr>';
            }   
            ?>

        </table>







        <!-- <button type="button" onclick="check()">Click Me!</button> -->
        <!-- <p id="demo">This is a demonstration.</p> -->
        <!-- <script>
        function check() { 
        document.getElementById("demo").innerHTML = "Thong bao: ";
        document.getElementById("tukhoa").value = "";
        }
        </script> -->
        <script>
        function xoa() { 
        document.getElementById("tukhoa").value = "";
        document.getElementById("phankhoa").value = "empty";
        }
        </script>
        



        




        
        



    </form>
</body>

</html>