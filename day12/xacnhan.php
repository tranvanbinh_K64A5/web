<?php 
    session_start(); 
    function reFormatDate($string) {
        $date_splitted = explode("/", $string);
        $date = "";
        $date = $date_splitted[2]."-".$date_splitted[1]."-".$date_splitted[0];
        return $date;
    }

    $hovaten = $_SESSION['hovaten'];
    $gioitinh = $_SESSION['gioitinh'];
    $phankhoa = $_SESSION['phankhoa'];
    $ngaysinh = reFormatDate($_SESSION['ngaysinh']);
    $diachi = $_SESSION['diachi'];
    $hinhanh = $_SESSION['hinhanh'];
?>


<!DOCTYPE html>
<html lang="vn">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstra Datepicker CSS -->
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
</head>

<body>

        <?php 
        $gioi_tinh = array(0 => 'Nam', 1=> 'Nữ');
        $phan_khoa = array(
            "empty" => "",
            "MAT" => "Khoa học máy tính",
            "KDL" => "Khoa học vật liệu");


        //xử lí sự kiện ấn nút xác nhận
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if(isset($_POST['submitted'])) {
                $servername = "localhost";
                $database = "laptrinhweb";
                $username = "root";
                $password = "";
                $conn = mysqli_connect($servername, $username, $password, $database);

                $sql = "INSERT INTO student VALUES 
                (0, 
                '$hovaten',
                $gioitinh, 
                '$phankhoa', 
                '$ngaysinh',
                '$diachi',
                '$hinhanh'  )";

                if (mysqli_query($conn, $sql)) {
                    // echo "Insert du lieu thanh cong";
                } else {
                        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                }
                mysqli_close($conn);

                echo "<script> location.href='complete_regist.php'; </script>";
                exit;        
            }
        }
        ?>

    <form
        style="border: 2px solid #4f7ba3; width: 600px; margin: auto; padding-bottom: 20px; padding-top: 20px; margin-top: 50px;"
        name="xacnhan" action=""  method="post">


        <div class="container" style="display: flex;  justify-content: center; ">
            <font face="Arial"
                style="text-align: center;  width: 120px; height: 40px; border: 2px solid #4f7ba3; padding: 10px; color: white;  background-color: #70ad47;">
                Họ và tên</font>

            <p style=" height: 35px; width: 330px; margin-left: 20px;  padding-top: 10px;">
                <?php  echo $hovaten; ?> </p>

        </div>


        <div class="container" style="display: flex;  justify-content: center;  ">
            <font face="Arial"
                style="text-align: center;  width: 120px; height: 40px;  border: 2px solid #4f7ba3; padding: 10px; color: white;  background-color: #70ad47;">
                Giới tính</font>
            <p style=" height: 35px; width: 330px; margin-left: 20px; padding-top: 10px;">
                <?php echo $gioi_tinh[$gioitinh]; ?> </p>
        </div>

        <div class="container" style="display: flex;  justify-content: center;   ">
            <font face="Arial"
                style="text-align: center;  width: 120px; height: 40px;  border: 2px solid #4f7ba3; padding: 10px; color: white;  background-color: #70ad47;">
                Phân khoa</font>
            <p style=" height: 35px; width: 330px; margin-left: 20px; padding-top: 10px;">
                <?php  echo $phan_khoa[$phankhoa]; ?> </p>
        </div>

        <div class="container" style="display: flex;  justify-content: center;  ">
            <font face="Arial"
                style="text-align: center; width: 120px; height: 40px;  border: 2px solid #4f7ba3; padding: 10px; color: white;  background-color: #70ad47;">
                Ngày sinh</font>
            <p style=" height: 35px; width: 330px; margin-left: 20px; padding-top: 10px;">
                <?php  echo $ngaysinh; ?></p>
        </div>


        <div class="container" style="display: flex;  justify-content: center;  ">
            <font face="Arial"
                style="text-align: center;  width: 120px; height: 40px;  border: 2px solid #4f7ba3; padding: 10px; color: white;  background-color: #70ad47;">
                Địa chỉ</font>
            <p style=" height: 35px; width: 330px; margin-left: 20px;   padding-top: 10px;">
                <?php echo $diachi; ?></p>
        </div>


        <div class="container" style="display: flex;  justify-content: center; ">
            <font face="Arial"
                style="text-align: center;  width: 120px; height: 40px;  border: 2px solid #4f7ba3; padding: 10px; color: white;  background-color: #70ad47;">
                Hình ảnh</font>
            <div style=" height: 35px; width: 330px; margin-left: 20px;  ">
                <img src="<?php  echo $hinhanh;?>" alt="chưa có ảnh" style=" height: 40px; width: 50px;">
            </div>
        </div>


        <div style="display: flex;align-items: center; margin-top: 20px;">
            <button type="submit" name="submitted"
                style="background-color: #70ad47; border: 2px solid #4f7ba3; color: white;  width: 120px;  height: 40px; margin: auto; border-radius: 8px; margin-top: 10px;">
                Xác nhận</button>
        </div>

    </form>


</body>

</html>