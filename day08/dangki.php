<?php
session_start();
?>


<!DOCTYPE html>
<html lang="vn">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstra Datepicker CSS -->
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
</head>

<body>

    <?php 
        //khai báo biến nhớ
        $hovaten =  $phankhoa = $ngaysinh = $diachi = "";
        $gioitinh = -1;

        //khai báo option
        $gioi_tinh = array(0 => 'Nam', 1=> 'Nữ');
        $phan_khoa = array(
            "empty" => "",
            "MAT" => "Khoa học máy tính",
            "KDL" => "Khoa học vật liệu");
        
        
        
        function checkDateFormat($string) {
            $date_splitted = explode("/", $string);
            $date = "";
            if (count($date_splitted) == 3){
                $date = $date_splitted[2]."/".$date_splitted[1]."/".$date_splitted[0];
            } else {
                return 0;
            }
            if (strtotime($date)) {
                return 1;
                }
            return 0;
        }


        function validateImage(){
            if (isset($_POST['diachi'])){
                if ( $_FILES["hinhanh"]["tmp_name"] != ""){
                    $isImage = getimagesize($_FILES["hinhanh"]["tmp_name"]);
                    if ($isImage == true){
                        return 1; #đúng ảnh
                    } else
                    return 0; #sai ảnh
                }
            }
            return -1; #rỗng
        }


        //hàm này sau khi thực hiện uplaod file lên server thì sẽ trả về đường dẫn tới ảnh trên server
        function uploadImage(){
            $dir = "upload";
            if(!file_exists($dir)){
                mkdir($dir);
            } 
            $target_dir = "upload/";
            $extension_file = pathinfo($_FILES['hinhanh']['name'], PATHINFO_EXTENSION);
            $basename_file = pathinfo($_FILES['hinhanh']['name'], PATHINFO_FILENAME); 
            $target_file = $target_dir . $basename_file .'_'. date("YmdHis").'.'.$extension_file;
            if (!file_exists($target_file)) {
                move_uploaded_file($_FILES["hinhanh"]["tmp_name"], $target_file);
            } 
            return $target_file;
        }


        //xử lí sự kiện ấn nút submit
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if(isset($_POST['submitted'])) {
                if ( isset($_POST['hovaten']) and isset($_POST['gioitinh']) and isset($_POST['phankhoa']) and isset($_POST['ngaysinh']) and validateImage() ==1){
                    if (checkDateFormat($_POST['ngaysinh']) == 1){
                        // $_SESSION['hovaten'] = $_POST['hovaten'];
                        // $_SESSION['gioitinh'] = $gioi_tinh[$_POST['gioitinh']];
                        // $_SESSION['phankhoa'] = $phan_khoa[$_POST['phankhoa']] ;
                        // $_SESSION['ngaysinh'] = $_POST['ngaysinh'];
                        // $_SESSION['diachi'] = "(chưa nhập địa chỉ)";
                        // if (isset($_POST['diachi']) == 1){
                        //     if (trim( $_POST['diachi']) != ""){
                        //         $_SESSION['diachi'] = trim( $_POST['diachi']);
                        //     }
                        // }
                        // $_SESSION['hinhanh'] =  uploadImage(); 
                        // echo "<script> location.href='xacnhan.php'; </script>";
                        // exit;


                        //chuyển về trang cũ
                        echo "<script> location.href='danhsachsinhvien.php'; </script>";
                        exit;
                    }
                }


            }
        }
        ?>


    <form style="border: 2px solid #4f7ba3; width: 600px; margin: auto; padding-bottom: 20px; margin-top: 50px;"
        name="dangki" action="" enctype="multipart/form-data" method="post">
        <?php 
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $hovaten = isset($_POST['hovaten']) ? trim( $_POST['hovaten']):"";
            $gioitinh = isset($_POST['gioitinh']) ? trim( $_POST['gioitinh']):-1;
            $phankhoa = isset($_POST['phankhoa']) ? trim( $_POST['phankhoa']):"";
            $ngaysinh = isset($_POST['ngaysinh']) ? trim( $_POST['ngaysinh']):"";
            $diachi = isset($_POST['diachi']) ? trim( $_POST['diachi']):"";
            $is_correct_date_format = checkDateFormat($ngaysinh);
            
            if (isset($_POST['hovaten'])==false  or trim($_POST["hovaten"]) == "")
            {
            echo '<p>
            <font face="Arial" style="text-align: center;  width: 100px;  color: red; margin-left: 80px;">
                Hãy nhập tên.</font></p> ';
            } 

            if (isset($_POST['gioitinh'])==false)
            {
            echo '<p>
            <font face="Arial" style="text-align: center;  width: 100px;  color: red; margin-left: 80px;">
                Hãy chọn giới tính.</font></p> ';
            } 
            
             if ( trim($_POST["phankhoa"]) == "empty")
            {
                echo '<p>
            <font face="Arial" style="text-align: center;  width: 100px;  color: red; margin-left: 80px;">
                Hãy chọn phân khoa.</font></p> ';
            } 

            if (isset($_POST['ngaysinh'])==false or trim($_POST["ngaysinh"]) == "")
            {
                echo '<p>
            <font face="Arial" style="text-align: center;  width: 100px;  color: red; margin-left: 80px;">
                Hãy nhập ngày sinh.</font></p> ';
            }

            if ($is_correct_date_format==0 and trim($_POST["ngaysinh"]) != "")
            {
                echo '<p>
            <font face="Arial" style="text-align: center;  width: 100px;  color: red; margin-left: 80px;">
                Hãy nhập ngày sinh đúng định dạng</font></p> ';
            }

            if ( validateImage() == -1  )
            {
                echo '<p>
            <font face="Arial" style="text-align: center;  width: 100px;  color: red; margin-left: 80px;">
                Hãy chọn hình ảnh</font></p> ';
            } else if (validateImage() == 0){
                echo '<p>
            <font face="Arial" style="text-align: center;  width: 100px;  color: red; margin-left: 80px;">
                Hãy chọn đúng hình ảnh</font></p> ';
            }
        }
    ?>

        <div class="container"
            style="display: flex;  justify-content: center;   align-items: center; margin-top: 20px;">
            <font face="Arial"
                style="text-align: center;  width: 120px; height: 40px; border: 2px solid #4f7ba3; padding: 10px; color: white;  background-color: #2e75b6;">
                Họ và tên<span style="color: red;">*</span></font>
            <input type="text" name="hovaten" value="<?php echo $hovaten;?>"
                style=" height: 35px; border: 2px solid #4f7ba3; width: 330px; margin-left: 20px;">
        </div>


        <div class="container"
            style="display: flex;  justify-content: center;   align-items: center; margin-top: 20px;">
            <font face="Arial"
                style="text-align: center;  width: 120px; height: 40px;  border: 2px solid #4f7ba3; padding: 10px; color: white;  background-color: #2e75b6;">
                Giới tính<span style="color: red;">*</span></font>
            <div style='width: 330px; margin-left: 20px; '>
                <?php 
                    $count = count($gioi_tinh);
                    for ($i = 0; $i < $count; $i++){
                    if ($gioitinh == $i){
                        echo '<input type="radio"  name="gioitinh" style="margin-left: 10px;" value = "',$i,'" checked> 
                      <label>',$gioi_tinh[$i],'</label>';
                    } else {
                        echo '<input type="radio"  name="gioitinh" style="margin-left: 10px;" value = "',$i,'"> 
                      <label>',$gioi_tinh[$i],'</label>';
                        }
                    }
                ?>
            </div>
        </div>

        <div class="container"
            style="display: flex;  justify-content: center;   align-items: center; margin-top: 20px;">
            <font face="Arial"
                style="text-align: center;  width: 120px; height: 40px;  border: 2px solid #4f7ba3; padding: 10px; color: white;  background-color: #2e75b6;">
                Phân khoa<span style="color: red;">*</span></font>
            <div style='width: 330px; margin-left: 20px; '>
                <select name="phankhoa" id="phan_khoa" style='width: 150px; height : 40px; border: 2px solid #4f7ba3;'
                    value="<?php echo $phankhoa;?>">
                    <?php 
                    $count = count($phan_khoa);
                    foreach($phan_khoa as $key => $value){
                        if ($key == $phankhoa){
                            echo '<option  selected="selected" value="',$key,'">',$value,'</option>';
                        } else {
                            echo '<option  value="',$key,'">',$value,'</option>';
                        }
                    }
                   ?>
                </select>
            </div>
        </div>

        <div class="container"
            style="display: flex;  justify-content: center;   align-items: center; margin-top: 20px;">
            <font face="Arial"
                style="text-align: center; width: 120px; height: 40px;  border: 2px solid #4f7ba3; padding: 10px; color: white;  background-color: #2e75b6;">
                Ngày sinh<span style="color: red;">*</span></font>
            <div style='width: 330px; margin-left: 20px; '>
                <input class="date form-control" type="text" name="ngaysinh" placeholder="dd/mm/yyyy"
                    value="<?php echo $ngaysinh;?>" style=" height: 40px; border: 2px solid #4f7ba3; width: 150px;">
            </div>
        </div>


        <div class="container"
            style="display: flex;  justify-content: center;   align-items: center; margin-top: 20px;">
            <font face="Arial"
                style="text-align: center;  width: 120px; height: 40px;  border: 2px solid #4f7ba3; padding: 10px; color: white;  background-color: #2e75b6;">
                Địa chỉ</font>
            <input type="text" name="diachi"
                style=" height: 40px; border: 2px solid #4f7ba3; width: 330px; margin-left: 20px;">
        </div>


        <div class="container"
            style="display: flex;  justify-content: center;   align-items: center; margin-top: 20px;">
            <font face="Arial"
                style="text-align: center;  width: 120px; height: 40px;  border: 2px solid #4f7ba3; padding: 10px; color: white;  background-color: #2e75b6;">
                Hình ảnh</font>
            <input type="file" name="hinhanh" style=" height: 30px;  width: 330px; margin-left: 20px;" />
        </div>

        <div style="display: flex;align-items: center; margin-top: 20px;">
            <button type="submit" name="submitted"
                style="background-color: #2e75b6; border: 2px solid #4f7ba3; color: white;  width: 120px;  height: 40px; margin: auto; border-radius: 8px; margin-top: 10px;">
                Đăng kí </button>
        </div>





    </form>

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.js"></script>
    <!-- Bootstrap Datepicker JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js">
    </script>
    <!-- Page Script -->
    <script type="text/javascript">
        $(document).ready(function () {
            // Cấu hình tiếng việt cho Bootstrap Datepicker
            $.fn.datepicker.dates['vi'] = {
                days: ["Chủ nhật", "Thứ Hai", "Thứ Ba", "Thứ Tư", "Thứ Năm", "Thứ Sáu", "Thứ Bảy"],
                daysShort: ["CN", "Thứ 2", "Thứ 3", "Thứ 4", "Thứ 5", "Thứ 6", "Thứ 7"],
                daysMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
                months: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7",
                    "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"
                ],
                monthsShort: ["Th 1", "Th 2", "Th 3", "Th 4", "Th 5", "Th 6", "Th 7", "Th 8", "Th 9",
                    "Th 10", "Th 11", "Th 12"
                ],
                today: "Hôm nay",
                clear: "Xóa",
                format: "dd/mm/yyyy",
                titleFormat: "MM yyyy",
                /* Cú pháp giống 'format' */
                weekStart: 0
            };
            // Thiết lập datepicker
            $('.date').datepicker({
                language: 'vi',
                todayHighlight: true, // nổi bật ngày hôm nay
                format: 'dd/mm/yyyy', // định dạng ngày trước khi gửi
                autoclose: true //Tự động đóng sau khi click chọn ngày
            });

            $("#btnSubmit").on("click", function () {
                var $this = $("#btnSubmit"); //ID nút gửi dữ liệu
                var $caption = $this.html(); // Nội dung html của nút gửi dữ liệu
                var form = "#form"; //Xác định #form ID
                var formData = $(form).serializeArray(); //Cho dữ liệu vào mảng
                var route = $(form).attr('action'); //lấy đường dẫn gửi dữ liệu

                // Ajax config
                $.ajax({
                    type: "POST", //Sử dụng phương thức POST để gửi duex liệu
                    url: route, // đường dẫn gửi dữ liệu
                    data: formData, // Dữ liệu theo mảng
                    beforeSend: function () { //Thêm thuộc tích disabled để chặn click nhiều lần trong khi đang gửi dữ liệu
                        $this.attr('disabled', true).html("Đang tiến hành...");
                    },
                    success: function (
                        response) { //Khi thành công kết quả sẽ được trả về tại đây
                        $this.attr('disabled', false).html($caption);
                        // Chúng ta sẽ thực hiện hành động sau khi thành công ở đây
                        console.log(response);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        // Bạn có thể thêm nội dung hiển thị báo lỗi tại đây
                    }
                });
            });


        });
    </script>
</body>

</html>