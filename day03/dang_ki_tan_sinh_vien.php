<!DOCTYPE html>
<html lang="vn">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<body>

    <form style="border: 2px solid #4f7ba3; width: 600px; height: 340px; margin: auto;">

        <div class="container"
            style="display: flex;  justify-content: center;   align-items: center; margin-top: 50px;">
            <font face="Arial"
                style="text-align: center;  width: 100px;  border: 2px solid #4f7ba3; padding: 10px; color: white;  background-color: #5b9bd5;">
                Họ và tên</font>
            <input type="text" name="tendangnhap" value=""
                style=" height: 40px; border: 2px solid #4f7ba3; width: 320px; margin-left: 20px;">
        </div>


        <div class="container"
            style="display: flex;  justify-content: center;   align-items: center; margin-top: 20px;">
            <font face="Arial"
                style="text-align: center;  width: 100px;  border: 2px solid #4f7ba3; padding: 10px; color: white;  background-color: #5b9bd5;">
                Giới tính</font>
            <div style='width: 330px; margin-left: 20px; '>
                <?php 
                    $gioi_tinh = array(
                    array("Nam", 0),
                    array("Nữ", 1));
                    foreach ($gioi_tinh as $gt) {
                    echo '<input type="radio" id=',$gt[1],' name="gioi_tinh" value="',$gt[0],'">
                      <label>',$gt[0],'</label>';
                    } ?>
            </div>
        </div>

        <div class="container"
            style="display: flex;  justify-content: center;   align-items: center; margin-top: 20px;">
            <!-- <p
                style="text-align: center;  width: 100px;  border: 2px solid #4f7ba3; padding: 10px; color: white;  background-color: #5b9bd5;">
                Phân khoa</p> -->
            <font face="Arial"
                style="text-align: center;  width: 100px;  border: 2px solid #4f7ba3; padding: 10px; color: white;  background-color: #5b9bd5;">
                Phân khoa</font>
            <div style='width: 330px; margin-left: 20px; '>
                <select name="phan_khoa" id="phan_khoa" style='width: 150px; height : 40px; border: 2px solid #4f7ba3;'>
                    <?php 
                    $phan_khoa = array(
                    array('','empty'),
                    array('Khoa học máy tính','MAT'),
                    array('Khoa học vật liệu','KDL'));
                    foreach ($phan_khoa as $khoa) {
                    echo '<option value="',$khoa[1],'">',$khoa[0],'</option>';
                    } ?>
                </select>
            </div>
        </div>

        <div style="display: flex;align-items: center; margin-top: 15px;">
            <button
                style="background-color: #70ad47; border: 2px solid #4f7ba3; color: white;  width: 120px;  height: 40px; margin: auto; border-radius: 8px; margin-top: 10px;">
                Đăng kí </button>
        </div>
    </form>

</body>

</html>